import pandas


def read_data_file(path, sheet_name, signals, cell_count_col):
    """ Reads an Excel file into a pandas DataFrame and deletes
        all columns that aren't either a signal intensity or
        the cell_count_col.
    """
    xls_file = pandas.ExcelFile(path)
    data = xls_file.parse(sheet_name)
    cols_to_keep = set(signals + [cell_count_col])
    for col in data.columns:
        if col not in cols_to_keep:
            del data[col]

    # Make sure the cell_count_col is always "cell_count"
    #
    data.rename(columns={cell_count_col: "cell_count"}, inplace=True)
    return data
