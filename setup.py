try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='lincs',
      version='0.0.1',
      description='LINCS project data analysis package',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 2.7',
      ],
      keywords='lincs',
      # url='http://github.com/storborg/funniest',
      author='Kyle Jensen',
      author_email='kljensen@gmail.com',
      # license='MIT',
      packages=['lincs'],
      install_requires=[
          'docopt',
          'numpy',
          'openpyxl',
          'xlrd',
          'pandas',
          'pyyaml',
          'pyparsing',
          'matplotlib',
      ],
      scripts=[
        'bin/parse_lincs_expt.py',
        'bin/analyze_lincs_treatments.py'
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      zip_safe=False
)
