.. LINCS Analysis documentation master file, created by
   sphinx-quickstart on Tue Nov 20 09:11:10 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LINCS Analysis's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 2
   
   getting_started.rst

   lincs.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

