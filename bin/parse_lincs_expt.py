"""Parse LINCS Experiment.

Usage:
  parse_lincs_expt.py [options] -c <config_file> -o <output_file>
  parse_lincs_expt.py -h | --help
  parse_lincs_expt.py --version

Options:
  -h --help     Show this screen.
  -v --verbose  Use verbose logging.
  --version     Show version.
"""
from docopt import docopt
import logging
import itertools
from lincs.io.config import read_config
from lincs.io.genepix import read_genepix
from lincs.io.elements import read_cell_counts
from lincs.analysis.elements import (
    join_custom_aligned_counts
)
from lincs.analysis.genepix import (
    extract_intensities,
    join_counts_and_intensities,
    align_all,
    add_sum_nonref_column,
    find_high_zero_cell
)


def setup_logging(verbose=False):
    """ Configure logging.

        :param verbose: Whether or not to use DEBUG log level
        :type verbose: bool.
        :returns:  None.
    """
    logging.basicConfig(format='%(filename)s %(levelname)s ' \
        + 'line %(lineno)d --- %(message)s',)
    if verbose:
        logging.root.setLevel(logging.DEBUG)
    else:
        logging.root.setLevel(logging.INFO)


def main(config, output_file):
    """ Main routine

        :param config: Dictionary with experiment configuration
        :type config: dict
        :param output_file: Path to file where .xlsx output should
         be written
        :type output_file: str
        :returns:  None.
    """

    # Read in the Elements file
    #
    elements_file = config['elements_file']['path']
    cell_counts = read_cell_counts(
        elements_file,
        config['elements_file']['sheets'],
        config['elements_file']['manual']
    )

    # Read in the Genepix file
    #
    genepix_file = config['genepix_file']['path']
    gpix = read_genepix(
                genepix_file,
                needed_sheets=itertools.chain.from_iterable(
                    config['genepix_file']['sheets'].values()
                ),
    )

    # Extract some config parameters
    #
    reference_ab = config['genepix_file']['reference_ab']
    flow_pattern = config['genepix_file']['flow_pattern']
    ab_channels = config['genepix_file']['ab_channels']
    num_columns = config['num_columns']
    antibodies = sorted(ab_channels.keys())
    nonref_antibodies = sorted(set(antibodies) - set([reference_ab]))

    #   Calculate genepix intensities
    #
    intensities = extract_intensities(
        config['genepix_file']['sheets'],
        gpix,
        num_columns,
        config['num_wells'],
        flow_pattern,
        reference_ab,
        ab_channels,
        num_end_wells_ignored=config.get('num_end_wells_ignored'),
        channel_shifts=config['genepix_file'].get('channel_shifts'),
        inverted=config['genepix_file'].get('inverted')
    )

    joined_intensities = join_counts_and_intensities(
        cell_counts,
        intensities
    )
    add_sum_nonref_column(
        joined_intensities,
        reference_ab,
        ab_channels
    )
    mappings, alignment_details, warnings = align_all(
        intensities,
        cell_counts,
        reference_ab,
        ab_channels,
        num_columns
    )
    joined_intensities = join_custom_aligned_counts(
        joined_intensities,
        cell_counts,
        mappings
    )
    joined_intensities.index.names = ["column", "well"]

    # Identify wells with zero cells and suspiciously
    # high intensities.
    #
    find_high_zero_cell(
        joined_intensities,
        nonref_antibodies,
        "cell_count_a",
        percentile=75
    )

    joined_intensities.to_excel(output_file)

    # import IPython; IPython.embed()
    return joined_intensities


if __name__ == '__main__':
    arguments = docopt(__doc__, version='Parse LINCS Experiment 0.1')
    setup_logging(verbose=arguments["--verbose"])
    config = read_config(arguments["<config_file>"])
    main(
        config,
        arguments["<output_file>"],
    )
