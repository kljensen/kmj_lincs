=====================
LINCS data processing
=====================

This respository contains software for processing some of the 
single-cell LINCS experiments from the 
`Miller-Jensen lab at Yale <http://miller-jensen.org>`_.

Installation
============

First, you should probably be using `virtualenv <https://pypi.python.org/pypi/virtualenv>`_
and `virtualenvwrapper <https://bitbucket.org/dhellmann/virtualenvwrapper>`_.  To install 
the LINCS package into your Python environment, run

	pip install numpy

	pip install -e 'git+https://kljensen@bitbucket.org/kljensen/kmj_lincs.git#egg=lincs'

Usage
=====

The main executable is ``parse_lincs_expt.py``, which is usually called
something like::

	parse_lincs_expt.py -c nef_config.yaml -o foo.xlsx

This script is used to join cell count data from
`NIS Elements <http://www.nikoninstruments.com/Products/Software/NIS-Elements-Microscope-Imaging-Software>`_ 
to intensity data from a
`Genepix scanner <http://www.moleculardevices.com/Products/Instruments/Microarray-Scanners/GenePix-4000B.html>`_.
You must, clearly, provide a configuration file.

There is also an analysis script that you can run like
	
	analyze_lincs_treatments.py -c 20130416_analysis_config.yaml -o tmp/bar2

Author
======
Kyle Jensen, kljensen@gmail.com
